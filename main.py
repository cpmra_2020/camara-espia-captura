import requests
import os
from gpiozero import Button, LED
from picamera import PiCamera
from time import sleep, strftime

url = 'http://192.168.1.64:3000/upload-photo'

camera = PiCamera()
camera.resolution = (2560, 1920)

#greenLed = LED(16)
#greenLed.on()
#redLed = LED(20)
#redLed.off()

led = LED(16)
led.on()

button = Button(12)
buttonLastState = False

def errorBlink():
    led.on()
    sleep(1)
    led.off()
    sleep(1)
    led.on()
    sleep(1)
    led.off()
    sleep(1)
    led.on()

def takePhotoAndUpload():
    time = strftime("%H-%M-%S")
    filename = f"/tmp/{time}.jpg"

    camera.capture(filename)

    files = {'photo': open(filename, 'rb')}

    try:
        r = requests.post(url, files=files, timeout=30)

        print(r.status_code)

        if (r.status_code == 200):
            print('success')
            led.on()
        else:
            errorBlink()
    except:
        errorBlink()
    os.remove(filename)

while True:
    if(buttonLastState != button.is_pressed):
        if button.is_pressed:
            led.off()
            sleep(3)
            led.on()
            sleep(1)
            led.off()
            buttonLastState = True
            takePhotoAndUpload()
        else: 
            buttonLastState = False
